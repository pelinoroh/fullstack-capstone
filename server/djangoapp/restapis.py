import requests
import json
from .models import CarDealer, DealerReview
from requests.auth import HTTPBasicAuth


# Create a `get_request` to make HTTP GET requests
def get_request(url,**kwargs):
    #if api_key:
    #    requests.get(url, params=params, headers={'Content-Type':'application/json'},auth=HTTPBasicAuth('apikey', api_key))
    #else:
    #    request.get(url, params=params)

    try:
        response = requests.get(url, headers={'Content-Type':'application/json'}, params=kwargs)
    except:
        print("busted")

    status_code = response.status_code
    json_data=json.loads(response.text)
    return json_data

# Create a `post_request` to make HTTP POST requests
def post_request(url,json_payload,**kwargs):
    response = requests.post(url, params=kwargs,json=payload)
    return response


# Create a get_dealers_from_cf method to get dealers from a cloud function
def get_dealers_from_cf(url, **kwargs):
    results = []
    json_result = get_request(url)
    if json_result:
        # Get dealers
        dealers = json_result["body"]["rows"]
        for dealer in dealers:
            dealer_doc = dealer["doc"]
            dealer_obj = CarDealer(address=dealer_doc["address"], city=dealer_doc["city"], full_name=dealer_doc["full_name"],
                                   id=dealer_doc["id"], lat=dealer_doc["lat"], long=dealer_doc["long"],
                                   short_name=dealer_doc["short_name"],
                                   st=dealer_doc["st"], zip=dealer_doc["zip"])
            results.append(dealer_obj)
    return results

# Create a get_dealer_reviews_from_cf method to get reviews by dealer id from a cloud function
def get_dealer_reviews_from_cf(url, **kwargs):
    results = []
    dealer_id = kwargs
    json_result = get_request(url, dealerId=dealer_id)
    if json_result:
        # Get dealers
        reviews = json_result["body"]["rows"]
        for review in reviews:
            review_doc = review["doc"]
            review_obj = DealerReview(dealership=review_doc["dealership"], name=review_doc["name"], purchase=review_doc["purchase"],
                                   id=review_doc["id"], sentiment="", review=review_doc["review"],
                                   purchase_date="",car_year="",
                                   car_make="", car_model="")
            results.append(review_obj)
    return results


def get_dealer_by_id_from_cf(url, dealerId):
    pass

# Create an `analyze_review_sentiments` method to call Watson NLU and analyze tex
def analyze_review_sentiments(dealerreview):
    params = dict()
    params["text"] = kwargs["text"]
    params["version"] = kwargs["version"]
    params["features"] = kwargs["features"]
    params["return_analyzed_text"] = kwargs["return_analyzed_text"]
    response = requests.get(url, params=params, headers={'Content-Type': 'application/json'}, auth=HTTPBasicAuth('apikey', api_key))
    get_request(url)

def post_request(url, payload, **kwargs):
    print(kwargs)
    response = requests.post(url, params=kwargsm, json=payload)
    status_code=response.status_code
    json_data = json.loads(response.text)
    return json_data
